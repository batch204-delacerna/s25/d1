//alert("Hello!");

/*
  JSON Objects
    JSON stands for JavaScript Object Notation
    JSON is also used in other programming languages.
    JavaScript Objects are not to be confused with JSON.
    A common use of JSON is to read data from a web server, and display the data in a web page.
    Features of JSON:
      - It is a lightweight data-interchange format.
      - It is easy to read and write.
      - It is easy for machines to parse and generate.

  
  Syntax:
    {
      "propertyA": "valueA",
      "propertyB": "valueB"
    }
*/

//JSON Objects
// - JSON is also use the "key/value pairs" just like the object properties in JavaScript.
// - "key/properties" names requires to be enclosed with double quotes.

// {
//  "city": "Quezon City",
//  "province": "Metro Manila",
//  "country": "Philippines"
// }

//JSON Array
// "cities": [
//  {
//    "city": "Quezon City",
//    "province": "Metro Manila",
//    "country": "Philippines"
//  },
//  {
//    "city": "Manila",
//    "province": "Metro Manila",
//    "country": "Philippines"
//  },
//  {
//    "city": "Makati City",
//    "province": "Metro Manila",
//    "country": "Philippines"
//  }
// ]

//JSON Methods
  //The JSON object contains methods for parsing and converting data into stringified JSON


//Converting Data into a stringified JSON

let batchesArr = [
  { batchName: "Batch 204"},
  { batchName: "Batch 203"}
];

console.log(batchesArr);
// The "stringfy" method is used to convert JavaScript Objects into a string.
console.log(JSON.stringify(batchesArr));

let data = JSON.stringify({
  name: 'John',
  age: 18,
  address: {
    city: 'Manila',
    country: 'Philippines'
  }
});

console.log(data);

//User Details
// let firstName = prompt("What is your first name?");
// let lastName = prompt("What is your last name?");
// let age = prompt("What is your age?");
// let address = {
//  city: prompt("Which city do you live in?"),
//  country: prompt("Which country does your city belong to?")
// };

// let otherData = JSON.stringify({
//  firstName: firstName,
//  lastName: lastName,
//  age: age,
//  address: address
// })

// console.log(otherData);

//Convert stringified JSON into JS Objects
  //JSON.parse() - to convert JSON Object into JavaScript Object
let batchesJSON = `[{"batchName": "Batch 204"}, {"batchName": "Batch 203"}]`;
console.log(batchesJSON);
console.log(JSON.parse(batchesJSON));

let stringifiedObject = `{
  "name": "John",
  "age": 18,
  "address": {
    "city": "Manila",
    "country": "Philippines"
  }
}`

console.log(JSON.parse(stringifiedObject));
